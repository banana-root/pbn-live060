---
layout: post
title: "K-드라마로 배우는 메타버스"
toc: true
---

 안녕하세요!
모두들 소설 연휴 다들 일쑤 보내셨나요? 🧧
저는 맛있는 것도 무지무지 묵고, 잠도 푸우욱 자고, 재미난 걸 무진 보았어요! 🤭
혹시 '숏폼 전성시대'라고 들어보셨나요? 요즘 MZ세대(그게바로접니다.)들은 짧은 시간안에 최대 많은 콘텐츠를 보길 원하고, 1시간 30분이 걸리는 극 어떤 편보단 10분으로 압축한 영상이나 예술 짤을 즐겨보곤하는데요. 이같은 숏폼의 인기는 코로나로 인해서 수많은 콘텐츠가 쏟아지면서 길이가 긴 콘텐츠에 대한 지루함이 커지고 자연스럽게 ‘짧게. 본론만.‘ 담는 숏폼이 인기를 끌고 있다는 분석이라고 합니다. 아.니.나.다.를.까. 저 게다가 인제 항맥 미뤄왔던~ 테두리 윤차 더욱더 보고싶었던~ 드라마들을 몰아보기(한드라마를 1-2시간으로 압축한 콘텐츠)를 시청하며 설날을 보냈습니다! 😎 나의 아저씨, Wendseday, 별에서 온 그대, 사랑의 불시착, W, 낌새 아이디는 강남미인, 환상의 짝꿍.. 알고리즘의 흐름을 타고 이정도 본 것 같아요.
그런데 수지 작품들 중앙 [별에서 온 그대]와 [W]에서 공통점을 찾아볼 핵 있었습니다! 이러한 점에서 두 극 모두 주인공이 시방 세계와 다른 세계를 오고 가는 것이였는데요! 이와 다름없이 메타버스 편성 여건 중간 거울세계와 가상세계를 살펴볼 수 있을 것 같아 준비해보았습니다. 🪄
 '메타버스의 유형은 크게 4가지로 나뉩니다..' 라고 이야기를 시작하면 지루하시겠죠? ㅎㅎ 넵! 따라서 저희는 금대 네 분기 중도 간단하게 거울세계(Mirror World)와 가상세계(Virtual World)를 알아보도록 할게요.
 미리미리 [별에서 온 그대]를 살펴보면 지구가 아닌 다른 별에서 온 외계인 도민준이 존재합니다. 저희는 도민준이 사는 세상을 가상세계(Virtual World)라고 생각하시면 됩니다! 메타버스라는 세상을 무척 거창하게 생각하실 필요가~ 없어요!! 도민준이 자신의 별을 왔다~갔다 하듯 저희도 메타버스란 가상세계를 왔다~갔다 하는 거라고 생각하시면 됩니다! 다른 예를 들어보자면 '동물의 숲'을 플레이 하는 것 뿐만 아니라 상정 세계를 왔다 갔다 하는 것처럼 말이죠.
 또한 희곡 [W]에서는 주인공이 워낙 '웹툰'이라는 가상 공간에 그려진 중심인물 강철이 실상 실태 세계를 넘나듭니다. 이런 점에서 극 눈발 계획 사실상 세계에서 어림생각 공간까지 이어질 복 있었던 이유는 곧 웹툰의 배경을 현실과 또옥같은 거울세계(Mirror World)로 그려놓았기 때문인데요, 이런 점에서 거울세계란 현실과 그저 구현해 놓은 세계를 이야기합니다. 현실적인 예시로 따지면 네이버 지도, 로드뷰, 어림생각 모델하우스 정도가 대표적일 것 같아요. 예시로 든 것들이 메타버스라는 게 웃기다구요? ㅎㅎ 그쯤 메타버스는 제자 곁에 흔하게 있는 서비스라고 생각하시면 됩니다. 모처럼 정형 서비스의 정의가 채 '메타버스'라는 한계 단어로 정의되는 과정일 뿐.. 우리 주목 앞에 보이는 하나의 공간을 오프라인 세계 뿐만이 아닌 온라인에서도 동일하게 경험하고 정보를 얻을 핵 있는 거죠. 그럼에도 불구하고 온라인에만 존재하는 공간을 오프라인에 구현하는 팝업스토어도 있답니다?
그런데 이익 드라마는 우리에게 메타버스가 보편화 됐을 때의 문제점도 아울러 비춰주었다고 생각합니다. 현실세계와 가상세계를 구분하지 못하는 순간이 온다면 극 속의 불행이 우리에게 닥칠지도 모르니까요,,
 위상 이야기는 언제까지나 메타버스에 대한 생각 의견이였습니다! 금번 편에서 예시를 든 [별에서 온 그대](2013)와 [W](2016) [메타버스](https://osexymeta.osexypartners.com) 두 줄거리 드라마를 살펴보자면, 방영된지 오래됐음에도 불구하고 현재의 핫이슈 '메타버스'의 성격을 담아내고 있음이 흥미로웠고, 이를 통해 '어렵게 생각했던 메타버스가 혹 우리 생애 근변 있던 건 아닐까?' 라는 생각을 다시 한 계단 해줄 복운 있게 해줘서 좋았던 것 같습니다.
설 연휴는 당기 지났지만.. 새해 복수 파다히 받으시고!! 당기 앞으로 휴일 많지 않은 2023을 보내야할 직딩이들을 응원하며..!🥹 다음에도 재미있는 이야기를 들고와보겠습니다! 그럼 안뇽 🙌🏻
